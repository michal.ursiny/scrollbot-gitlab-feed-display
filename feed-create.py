#!/usr/bin/env python

import sqlite3

conn = sqlite3.connect('feed.db')
print "Opened database successfully";

conn.execute('''CREATE TABLE NEWS
       (_ID INTEGER PRIMARY KEY AUTOINCREMENT,
        ID TEXT NOT NULL,
        MESSAGE TEXT NOT NULL,
        FLAG INTEGER NOT NULL DEFAULT 0,
        CONSTRAINT ID_UNIQUE UNIQUE (ID));''')
print "Table created successfully";

conn.close()
