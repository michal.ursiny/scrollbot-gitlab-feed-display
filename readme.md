Gitlab feed display scripts for ScrollBot
=========================================

This is set of scripts allowing you to display Gitlab Activity feed on ScrollBot by Pimoroni using their provided library.

See ScrollBot: https://shop.pimoroni.com/products/scroll-bot-pi-zero-w-project-kit

![gitlab-scrollbot](/uploads/8e7923095dab36b8325220338fad3a3c/gitlab-scrollbot.gif)

how it works
------------

1. `feed-create.py` will create sqlite db for you - you need to run it only once

2. `feed-fetch.py` will download your gitlab activity feed and put it into db

3. `feed-display.py` will take one item from db and sends it to scrollbot

how to set it up
----------------

0. clone this to your scrollbot, make sure you have all dependencies

1. get you gitlab feed url and edit the url in `feed-fetch.py`

2. create a cron job on your scrollbot, which will run every minute running `feed-fetch.py` and `feed-display.py` in sequence, this will make it download/check for new items and push items which hasn't been displayed yet to scrollbot

ENJOY!