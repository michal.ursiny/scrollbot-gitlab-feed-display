#!/usr/bin/env python

import time
import feedparser
import sqlite3

conn = sqlite3.connect('feed.db')

print "Opened database successfully";
print "Download feed"

requestUrl = "https://gitlab.com/michal.ursiny/scrollbot-gitlab-feed-display.atom?rss_token=pAJ4mgm62syWjxqA4CDC"

d = feedparser.parse(requestUrl)
for post in d.entries:
    hour = str(int(time.strftime('%H', post.updated_parsed)) + 1)
    minute = time.strftime('%m', post.updated_parsed)
    formatted_time = hour + ':' + minute
    #formatted_time = time.strftime('%H:%M', post.updated_parsed)
    print post.id + " " + formatted_time + " " + post.title
    conn.execute("INSERT OR IGNORE INTO NEWS (ID,MESSAGE) VALUES ('" + post.id + "', '" + formatted_time + " " + post.title + "')");

conn.commit()    
conn.close()    
