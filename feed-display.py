#!/usr/bin/env python

import time
import scrollphathd
from scrollphathd.fonts import font5x7
import sqlite3

conn = sqlite3.connect('feed.db')

print "Opened database successfully";
print "Display feed"

cursor = conn.execute("SELECT _ID, MESSAGE FROM NEWS WHERE FLAG = 0 ORDER BY _ID DESC LIMIT 1")
for row in cursor:
    print "_id = ", row[0]
    print "message = ", row[1]
    message = "      GITLAB ACTIVITY ALERT: " + row[1]
    conn.execute("UPDATE NEWS SET FLAG = 1 WHERE _ID = " + str(row[0]));
conn.commit()    
conn.close()    
    
scrollphathd.rotate(180)

length = scrollphathd.write_string(message, x=0, y=0, font=font5x7, brightness=0.5)
print "length = " + str(length)

for scroll in range(length):
    scrollphathd.show()
    scrollphathd.scroll(1)
    time.sleep(0.01)
